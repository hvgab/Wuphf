
# WUPHF

Navn stjålet fra The Office.

For å kunne sende meldinger for DRS i relevante kanaler for andre jobber som står og går.

## Quickstart

```python
from wuphf import Wuphf, Msg

wuphf = Wuphf()

msg = Msg(title='title', msg='message'))
wuphf.slack(msg, ())
wup
```

## Services:

- Basic SMTP mail
- Det mail-APIet.
- Purecloud
- Slack
- MS Teams
- DRSSMS
